const mongoose = require('mongoose');
const Book = mongoose.model('Book', new mongoose.Schema({
    bookName: {type: String},
    author: {type: String},
    publisher: {type: String},
    language: {type: String},
    category: {type: String},
    publishedYear: {type: String},
    isbnNo: {type: String},
    description: {type: String},
    bookStatus: {type: String},
    fileString: {type: String},
    thumbnailString: {type: String},
    scrapingId: {type: String},
    updatedDateTime: {type: Date},
    bookType: {type: String},
}));

module.exports.updateBooks = (books, scrapingId) => {
    books.forEach((book, index) => {
        if (null != book.fileString && book.fileString != '') {
            Book.findOneAndUpdate({bookName: book.bookName}, {
                    $set: {
                        bookName: book.bookName,
                        author: book.author,
                        publisher: book.publisher,
                        language: book.language,
                        publishedYear: book.publishedYear,
                        isbnNo: book.isbnNo,
                        description: book.description,
                        fileString: book.fileString,
                        thumbnailString: book.thumbnailString,
                        category: book.category,
                        scrapingId: scrapingId,
                        bookStatus: book.bookStatus,
                        bookType: book.bookType,
                        updatedDateTime: new Date().toLocaleString("en-US", {timeZone: 'Asia/Colombo'}),
                    }
                },
                {upsert: true, new: true, setDefaultsOnInsert: true},
                function (err, sLog) {
                    if (err) {
                        console.log(err)
                        return false;
                    }
                    return true;
                });
        }
    });
    return true;
};

