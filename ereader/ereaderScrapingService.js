const config = require('../config.js');
const request = require('request-promise-native');
const puppeteer = require('puppeteer-extra');
const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker');


module.exports.getBookList = (reqCategory) => {
    return new Promise((resolve, reject) => {
        puppeteer.use(AdblockerPlugin())
        puppeteer.launch({headless: true}).then(async browser => {
                let bookList = [];
                const page = await browser.newPage();

                await page.setViewport({width: 1366, height: 768});
                await page.goto(config.scrapping_url + 'findbook?genre=' + reqCategory);

                await page.setDefaultNavigationTimeout(0);
                await page.waitForTimeout(3000);

                const hrefs = await page.$$eval('div#contentholder_without_sidebar>table>tbody>tr>td:nth-child(1)>a', as => as.map(a => a.href));
                console.log(hrefs.length + " Books found.");

                for (let i = 0; i < hrefs.length; i++) {
                    console.log("Book " + i + " is scraping started.");

                    const book = {};
                    await page.goto(hrefs[i]);
                    await page.waitForTimeout(2000);

                    const imageUrl = await page.$$eval('div.container>div.book>div.row>div.col-lg-2>img', as => as.map(a => null != a ? a.getAttribute('src') : 'N/A'));

                    if ('N/A' != imageUrl) {
                        request({
                            url: config.scrapping_url + imageUrl.toString(),
                            method: 'GET',
                            encoding: null
                        }).then(result => {
                            let imageBuffer = Buffer.from(result);
                            let imageBase64 = imageBuffer.toString('base64');
                            let imageDataUrl = 'data:image/jpg;base64,' + imageBase64;
                            book.thumbnailString = imageDataUrl;
                        }).catch((err) => {
                            book.thumbnailString = 'N/A';
                        });
                    } else {
                        book.thumbnailString = 'N/A';
                    }

                    await page.evaluate(() => document
                        .querySelector('div.container>div.book>div.row>div.col-lg-10>div.row>div.col-md-9>h1')
                        .textContent.trim()).then((name) => {
                        book.bookName = name;
                    });

                    await page.waitForTimeout(2000);
                    const author = await page
                        .$$eval('div.container>div.book>div.row>div.col-lg-10>div.row:nth-child(2)>div:nth-child(1)>h4',
                            as => as.map(a => null != a ? a.textContent.trim() : 'N/A'));
                    book.author = author.toString();

                    await page.waitForTimeout(2000);
                    const category = await page
                        .$$eval('div.container>div.book>div.row>div.col-lg-10>div.row:nth-child(2)>div:nth-child(2)>h4>p>a',
                            as => as.map(a => null != a ? a.textContent.trim() : 'N/A'));
                    book.category = category.toString().charAt(0).toUpperCase() + category.toString().slice(1);

                    await page.waitForTimeout(2000);
                    await page.evaluate(() => document
                        .querySelector('div.container>div.book>div.row>div.col-lg-10>div.row:nth-child(4)>div>p.text-justify')
                        .textContent.trim()).then((description) => {
                        book.description = description;
                    });


                    book.fileString = 'https://english-e-reader.net/download?link=' + hrefs[i].replace('https://english-e-reader.net/book/', '') + '&format=epub';

                    book.language = "English";
                    book.publishedYear = "N/A";
                    book.publishedYear = "N/A";
                    book.isbnNo = "N/A";
                    book.publisher = "N/A";
                    book.bookStatus = "ACTIVE";
                    book.bookType = "AUTO";

                    bookList.push(book);
                    console.log("Book " + i + " is scraping ended.");
                }

                await page.close();
                resolve(bookList);

                await browser.close();

            }
        ).catch((er) => {
            console.log("Eror", er);
            reject(er);
        })

    })
}

module.exports;

