const config = require('../config');
const ereaderScrapingService = require('./ereaderScrapingService');
const scrapingLogService = require('../scrapingLogService');
const ereaderService = require('./ereaderService');

module.exports.getBookList = (category) => {

    let logPromise = scrapingLogService.createMappingLog({
        startDateTime: new Date().toLocaleString("en-US", {timeZone: 'Asia/Colombo'}),
        status: "INPROGRESS"
    });
    logPromise.then((log) => {
        let bookPromise = ereaderScrapingService.getBookList(category);
        bookPromise.then((books) => {
            if (books.length > 0) {
                if (ereaderService.updateBooks(books, log._id)) {

                    scrapingLogService.updateMappingLog({
                        endDateTime: new Date().toLocaleString("en-US", {timeZone: 'Asia/Colombo'}),
                        status: "COMPLETE",
                        scrapedBookCount: books.length
                    }, log._id);
                    console.log('info', 'Book List Updated on DB', {siteName: config.scrapping_url});
                } else {
                    scrapingLogService.updateMappingLog({
                        endDateTime: new Date().toLocaleString("en-US", {timeZone: 'Asia/Colombo'}),
                        status: "INCOMPLETE",
                        scrapedBookCount: books.length
                    }, log._id);
                    console.log('error', 'Error while updating to DB', {siteName: config.scrapping_url});
                }
            } else {
                scrapingLogService.updateMappingLog({
                    endDateTime: new Date().toLocaleString("en-US", {timeZone: 'Asia/Colombo'}),
                    status: "INCOMPLETE",
                    scrapedBookCount: books.length
                }, log._id);
                console.log('error', 'prices list is empty', {siteName: config.scrapping_url});
            }
        }).catch((error) => {
            scrapingLogService.updateMappingLog({
                endDateTime: new Date().toLocaleString("en-US", {timeZone: 'Asia/Colombo'}),
                status: "INCOMPLETE",
            }, log._id);
        });
    })

};
