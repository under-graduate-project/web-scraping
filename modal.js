const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let ModalSchema = new Schema(
    {
        // _id : {type : new ObjectID(), ref: 'Conditions'},
        bookName:{type: String, required: true},
        author:{type: String, required: true},
        publisher:{type: String, required: true},
        language:{type: String, required: true},
        category:{type: String},
        publishedYear: {type: String, required: true},
        isbnNo: {type: String, required: true},
        description: {type: String, required: true},
        fileString: {type: String},
        thumbnailString: {type: String, required: true}


    },
    { timestamps: true }
);

module.exports = mongoose.model('modal', ModalSchema);

