const express = require('express');
const mongoose = require('mongoose');
const config = require('./config.js');
const ereaderController = require('./ereader/ereaderController');
const cron = require("node-cron");


// port number
var port = 5057;
const app = express();

// database connection
mongoose.connect(config.dev_db_url, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
let db = mongoose.connection;

// inform if db connection error
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

function runCron(category) {
    ereaderController.getBookList(category);
}

/**
 *  Cron will run on every monday 12 AM
 */
cron.schedule("00 00 * * 1", function () {
    runCron("Horror");
});

/**
 *  Cron will run on every monday 01 AM
 */
cron.schedule("00 01 * * 1", function () {
    runCron("Adventure");
});

/**
 *  Cron will run on every monday 02 AM
 */
cron.schedule("00 02 * * 1", function () {
    runCron("biography");
});

/**
 *  Cron will run on every monday 03 AM
 */
cron.schedule("00 03 * * 1", function () {
    runCron("romance");
});

/**
 *  Cron will run on every monday 04 AM
 */
cron.schedule("00 04 * * 1", function () {
    runCron("comedy");
});

/**
 *  Cron will run on every monday 05 AM
 */
cron.schedule("00 05 * * 1", function () {
    runCron("Thriller");
});


// server
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`Book Scraper server is listening on ${port}`)
});

app.get('/scraper', (request, res) => {
    runCron(request.query.category);
    res.end("Scraping Process Started!");
});

app.get('/', (request, res) => {
    res.end("Scraping service up and running!");
});
