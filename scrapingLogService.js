const mongoose = require('mongoose');
const ScrapingLog = mongoose.model('ScrapingLog', new mongoose.Schema({
        startDateTime:{type: Date},
        endDateTime:{type: Date},
        status:{type: String},
        scrapedBookCount:{type: Number},
        
}));

module.exports.createMappingLog = (log) => {
    return new Promise((resolve, reject) =>{
        ScrapingLog.create(log, function(err,sLog){
            if(err){
                reject(err)
            }
            resolve(sLog);
        });

    });
};


module.exports.updateMappingLog = (log, scrapingId) => {
    ScrapingLog.findOneAndUpdate({_id:scrapingId},{$set:{endDateTime:log.endDateTime, status:log.status, scrapedBookCount:log.scrapedBookCount}}, function(err,sLog){
        if(err){
            console.log(err)
            return false;
        }
        return true;
    });
};

